BEGIN;

CREATE TABLE session_invitation (
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    subject_id UUID NOT NULL REFERENCES subject(uuid) ON DELETE CASCADE,
    object_id UUID,
    object_type TEXT,
    date_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    date_expires TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    token TEXT NOT NULL,
    action_path TEXT
);

ALTER TABLE session_invitation ADD CONSTRAINT object_reference CHECK (
    (object_id IS NOT NULL AND object_type IS NOT NULL) OR (object_id IS NULL AND object_type IS NULL)
);

COMMIT;

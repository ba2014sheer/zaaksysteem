BEGIN;

    INSERT INTO queue (type, label, priority, metadata, data)
        SELECT
            'touch_casetype',
            'devops: fix case uuid mismatch',
            3000,
            '{"require_object_model":1,"disable_acl":1,"target":"backend"}',
            '{"id":' || zaaktype_id || '}'
        FROM zaaktype_node
        WHERE created >= '2018-10-09'
        GROUP BY zaaktype_id;

    INSERT INTO queue (type, label, priority, metadata, data)
        SELECT
            'touch_case',
            'devops: fix case uuid mismatch',
            3000,
            '{"require_object_model":1,"disable_acl":1,"target":"backend"}',
            '{"case_object_id":"' || uuid || '"}'
        FROM object_data
        WHERE object_class = 'case'
            AND EXISTS (
                SELECT 1 FROM zaak WHERE id = object_data.object_id AND uuid != object_data.uuid
            );

    DELETE FROM case_property WHERE id IN (
        SELECT cp.id
        FROM case_property cp
        JOIN zaak z ON cp.case_id = z.id
        WHERE cp.object_id != z.uuid
    );

    UPDATE zaak SET uuid = (SELECT uuid FROM object_data WHERE object_id = zaak.id AND object_class = 'case');

COMMIT;

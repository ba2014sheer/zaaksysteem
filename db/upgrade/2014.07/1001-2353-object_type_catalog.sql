BEGIN;

-- Safe because the contents of this table are transient anyway
DROP TABLE IF EXISTS object_type_bibliotheek_entry;

CREATE TABLE object_type_bibliotheek_entry (
    id SERIAL PRIMARY KEY,
    search_term TEXT NOT NULL,
    object_type TEXT NOT NULL DEFAULT 'type',
    bibliotheek_categorie_id INTEGER NOT NULL,
    object_uuid UUID NOT NULL,
    CONSTRAINT bibliotheek_categorie_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie (id) ON DELETE CASCADE,
    CONSTRAINT object_data FOREIGN KEY (object_uuid) REFERENCES object_data (uuid) ON DELETE CASCADE
);

COMMIT;

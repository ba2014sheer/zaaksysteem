BEGIN;
    CREATE INDEX object_data_case_casetype_id_idx ON object_data( (index_hstore->'case.casetype.id') );
    CREATE INDEX object_data_case_requestor_id_idx ON object_data( (index_hstore->'case.requestor.id') );
COMMIT;

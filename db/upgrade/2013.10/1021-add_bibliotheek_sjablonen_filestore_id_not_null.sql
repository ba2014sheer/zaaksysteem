BEGIN;

-- handy to have, however, if there's null rows may not work on some db's.
alter table bibliotheek_sjablonen alter filestore_id set not null;

COMMIT;

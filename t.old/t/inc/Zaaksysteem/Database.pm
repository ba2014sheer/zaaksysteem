package Zaaksysteem::Database;
use Moose;
use namespace::autoclean;
use Perl::Version;

use DBI;

has username => (
    is  => 'ro',
    isa => 'Maybe[Str]',
);

has password => (
    is  => 'ro',
    isa => 'Maybe[Str]',
);

has host => (
    is  => 'ro',
    isa => 'Maybe[Str]',
);

has port => (
    is  => 'ro',
    isa => 'Maybe[Int]',
);

has template1 => (
    is      => 'ro',
    builder => '_connect_template1',
    lazy    => 1,
);

has pg_version => (
    is      => 'ro',
    builder => '_get_current_version',
    lazy    => 1,
);

sub create {
    my $self = shift;
    my $opts = {@_};

    return 1 if $self->_has_database($opts->{name});

    if ($opts->{template}) {
        $self->_create_db($opts->{name}, template => $opts->{template});
    }
    else {
        $self->_create_db($opts->{name});
        my $dbh = $self->_connect_dbi(name => $opts->{name});
        $dbh->do($opts->{sql});
        $dbh->disconnect;
    }

    return 1;
}

sub create_from_template {
    my $self = shift;
    my $opts = {@_};

    return 1 if $self->_has_database($opts->{name});
}

sub _has_database {
    my ($self, $name) = @_;

    my @res
        = $self->template1->selectrow_array(
        "SELECT datname FROM pg_database WHERE datname = ?",
        {}, ($name));
    return 1 if (@res);
    return 0;
}

sub _get_current_version {
    my $self = shift;
    my @res = $self->template1->selectrow_array("SELECT version()", {}, ());
    die "Unable to determine Postgres version " if (!@res);
    my $version = $res[-1];
    if ($version =~ /PostgreSQL\s+([\w+\.]+)\s+/) {
        $version = $1;
    }
    return Perl::Version->new($version);
}

sub _remove_other_connections {
    my ($self, $name) = @_;

    my $pid = $self->pg_version < Perl::Version->new('9.2') ? 'procpid' : 'pid';

    my $sql = qq{
        SELECT
            pg_terminate_backend($pid)
        FROM
            pg_stat_activity
        WHERE
            $pid <> pg_backend_pid()
        AND
            datname = ?
    };

    my @res = $self->template1->selectrow_array($sql, {}, ($name));
    return 1;
}

sub delete {
    my $self = shift;
    my $opts = {@_};

    return 1 if !$self->_has_database($opts->{name});

    $self->_remove_other_connections($opts->{name});
    $self->_remove_db($opts->{name});
    return 1;
}

sub _connect_template1 {
    my $self = shift;
    return $self->_connect_dbi(name => 'template1',);
}

sub _connect_dbi {
    my $self = shift;
    my $opts = {@_};

    if (!exists $opts->{connect_args}) {
        $opts->{connect_args} = {};
    }

    my $dsn = sprintf(
        'dbi:Pg:dbname=%s%s%s',
        $opts->{name},
        $self->host ? ";host=" . $self->host : "",
        $self->port ? ";port=" . $self->port : ""
    );

    return DBI->connect(
        $dsn,
        $self->username || undef,
        $self->password || undef,
        {
            RaiseError => 1,
            PrintError => 0,
            AutoCommit => 1,
        },
    );
}

sub _create_db {
    my ($self, $dbname, %opts) = @_;

    my $sql = sprintf('CREATE DATABASE "%s" ENCODING \'UTF-8\'', $dbname);
    if ($opts{template}) {
        $sql = sprintf('CREATE DATABASE "%s" WITH TEMPLATE "%s"', $dbname, $opts{template});
    }
    return $self->template1->do($sql);
}

sub _remove_db {
    my ($self, $dbname) = @_;
    $self->template1->do(sprintf('DROP DATABASE "%s"', $dbname));
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 NAME

Zaaksysteem::Database - Create an database from another database

=head1 SYNOPSIS

    use Zaaksysteem::Database;

    my $dbh = Zaaksysteem::Database->new(
        username => $username,
        password => $password,
        host     => $hostname,
        port     => $port,
    );

    $dbh->create(
        name => $database_name,
        sql  => $sql, # with all the create and insert statements
    );

    $dbh->delete(
        name => $database_name,
    );

=head1 DESCRIPTION

Generates a Zaaksystem database

=head1 METHODS

=head2 create

Creates the database and inserts the proper SQL in order to have a go

=head3 ARGUMENTS

=over

=item * name

The name of the database

=item * sql

The SQL needed to populate the database

=back

=head3 RETURNS

True on succes, false otherwise

=head2 delete

Deletes the database. Connections are dropped before deleting it, so beware.

=head3 ARGUMENTS

=over

=item * name

The name of the database

=back

=head3 RETURNS

True on succes, false otherwise

=head1 BUGS

None. Ever.

=head1 AUTHOR

    Wesley Schwengle
    Mintlab B.V.
    wesley@mintlab.nl
    http://mintlab.nl

=head1 SEE ALSO

Zaaksysteem

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

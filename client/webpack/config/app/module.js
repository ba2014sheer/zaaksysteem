const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const cherryPack = require('../../library/cherryPack');
const { IS_DEBUG_BUILD } = require('../../library/constants');

const extractStyle = use =>
  ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use,
  });

const exclude = /node_modules/;

module.exports = {
  rules: [
    {
      test: /.*\.js$/,
      loader: 'babel-loader',
      exclude: [exclude, /frontend/],
      options: {
        presets: ['babel-preset-es2015'].map(require.resolve),
        plugins: ['babel-plugin-add-module-exports'].map(require.resolve),
      },
    },
    {
      test: /.*\.html$/,
      // removing optional tags breaks HtmlWebpackPlugin script injection
      loader: 'html-loader?removeOptionalTags=false&removeDefaultAttributes=false',
      exclude,
    },
    {
      test: /\.scss$/,
      use: cherryPack({
        production() {
          return extractStyle([
            {
              loader: 'css-loader',
              options: {
                minimize: false,
                sourceMap: IS_DEBUG_BUILD,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [autoprefixer],
                sourceMap: IS_DEBUG_BUILD,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: IS_DEBUG_BUILD,
              },
            },
          ]);
        },
        development() {
          return [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [autoprefixer],
                sourceMap: true,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
              },
            },
          ];
        },
      }),
      exclude,
    },
    {
      test: /\.css$/,
      loader: cherryPack({
        production() {
          return extractStyle(['css-loader']);
        },
        development: 'style-loader!css-loader',
      }),
      exclude,
    },
    {
      test: /\.(woff(2)?|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url-loader?name=[name].[ext]&limit=10000'
    },
    {
      test: /\.swig$/,
      loader: 'swig-loader',
      exclude,
    }
  ],
  noParse: [
    /[/\\]node_modules[/\\](angular-mocks|angular-ui-router)/,
    /[/\\]node_modules[/\\]api-check/,
    /[/\\]node_modules[/\\]define/,
    /[/\\]node_modules[/\\]email-regex/,
    /[/\\]node_modules[/\\]fuzzy/,
    /[/\\]node_modules[/\\]iban/,
    /[/\\]node_modules[/\\]is-promise/,
    /[/\\]node_modules[/\\]match-media/,
    /[/\\]node_modules[/\\]openlayers/,
    /[/\\]node_modules[/\\]soma-events/,
    /[/\\]node_modules[/\\]word-ngrams/
  ],
};

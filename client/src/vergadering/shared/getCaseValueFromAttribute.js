/**
 * @param {string} attribute
 * @param {Object} instance
 * @return {string}
 */
function resolve(attribute, caseItem) {
  const { instance } = caseItem;

  switch (attribute) {
  case 'vertrouwelijkheid':
    return instance.confidentiality.mapped;
  case 'aanvrager_naam': {
    const {
      first_names,
      surname,
    } = instance.requestor.instance.subject.instance;

    return `${first_names} ${surname}`;
  }
  case 'zaaktype':
    return instance.casetype.instance.name;
  case 'zaaknummer':
    return caseItem.instance.number;
  default:
    return instance.attributes[attribute];
  }
}

/**
 * @param {string} attribute
 * @param {Object} caseItem
 * @return {string}
 */
export default function getCaseValueFromAttribute(attribute, caseItem) {
  const value = resolve(attribute, caseItem);

  return String(value).toLowerCase();
}

import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import mutationServiceModule from './../../../../shared/api/resource/mutationService';
import snackbarServiceModule from './../../../../shared/ui/zsSnackbar/snackbarService';
import propCheck from './../../../../shared/util/propCheck';
import sortBy from 'lodash/sortBy';
import findIndex from 'lodash/findIndex';
import assign from 'lodash/assign';
import pick from 'lodash/pick';
import omit from 'lodash/omit';
import get from 'lodash/get';
import shortid from 'shortid';

export default
	angular.module('caseRelationActions', [
		angularUiRouterModule,
		mutationServiceModule,
		snackbarServiceModule
	])
		.factory('caseRelationActions', [ '$state', 'snackbarService', ( $state, snackbarService ) => {

			return [
				{
					type: 'case/relation/add',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseReference: propCheck.string,
								relatedCaseReference: propCheck.string
							}),
							mutationData
						);

						return {
							url: `/api/v1/case/${mutationData.caseReference}/relation/add`,
							data: {
								related_id: mutationData.relatedCaseReference
							}
						};

					},
					reduce: ( data, mutationData ) => {
						return data.map(
							caseObj => {

								return caseObj.setIn(
									'instance.case_relationships.plain.instance.rows'.split('.'),
									get(caseObj.instance.case_relationships.plain, 'instance.rows', [])
										.concat({
											reference: mutationData.relatedCaseReference,
											type: 'case'
										})
								);
							}
						);
					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'Zaakrelatie wordt aangemaakt',
							{
								promise,
								catch: ( ) => {

								},
								then: ( ) => {

								}
							}
						);

					}
				},
				{
					type: 'case/relation/remove',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseReference: propCheck.string,
								relatedCaseReference: propCheck.string
							}),
							mutationData
						);

						return {
							url: `/api/v1/case/${mutationData.caseReference}/relation/remove`,
							data: {
								related_id: mutationData.relatedCaseReference
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj	=> {

								return caseObj.setIn(
									'instance.case_relationships.plain.instance.rows'.split('.'),
									get(caseObj.instance.case_relationships.plain, 'instance.rows', [])
										.filter(rel => rel.reference !== mutationData.relatedCaseReference)
								);
							}
						);

					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'Zaakrelatie wordt verwijderd',
							{
								promise,
								catch: ( ) => {

								},
								then: ( ) => {

								}
							}
						);

					}
				},
				{
					type: 'case/relation/move',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseReference: propCheck.string,
								relatedCaseReference: propCheck.string,
								afterCaseReference: propCheck.any.optional
							}),
							mutationData
						);

						return {
							url: `/api/v1/case/${mutationData.caseReference}/relation/move`,
							data: {
								related_id: mutationData.relatedCaseReference,
								after: mutationData.afterCaseReference
							}
						};
					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {

								return caseObj.merge({
									instance: {
										relations: {
											instance: {
												rows: sortBy(
													caseObj.instance.relations.instance.rows,
													( relatedCase ) => {

														let index = caseObj.instance.relations.instance.rows.indexOf(relatedCase);

														return relatedCase.reference === mutationData.relatedCaseReference ?
															(mutationData.afterCaseReference ?
																(findIndex(caseObj.instance.relations.instance.rows, { reference: mutationData.afterCaseReference }) * 10) + 1
																: -1
																)
															: index * 10;

													}
												)
											}
										}
									}
								}, { deep: true });

							}
						);

					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait('De volgorde van de zaken wordt aangepast', {
							collapse: 0,
							promise,
							then: ( ) => '',
							catch: ( ) => 'De volgorde van de zaken kon niet worden aangepast. Neem contact op met uw beheerder voor meer informatie.'
						});

					}
				},
				{
					type: 'case/relation/add/object',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseReference: propCheck.string,
								relatedObjectReference: propCheck.string,
								relatedObjectType: propCheck.string,
								relatedObjectLabel: propCheck.string
							}),
							mutationData
						);

						return {
							url: `/api/object/${mutationData.relatedObjectReference}/relate`,
							data: {
								related_object_id: mutationData.caseReference
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {

								return caseObj.merge({
									related_objects: caseObj.related_objects.concat(
										{
											relationship_name_a: 'related',
											relationship_name_b: 'related',
											blocks_deletion: false,
											owner_object_id: null,
											related_object: {
												label: mutationData.relatedObjectLabel
											},
											related_object_id: mutationData.relatedObjectReference,
											related_object_type: mutationData.relatedObjectType
										}
									)
								});

							}
						);
					},
					options: {
						reloadOnComplete: true
					}

				},
				{
					type: 'case/relation/remove/object',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseReference: propCheck.string,
								relatedObjectReference: propCheck.string
							}),
							mutationData
						);

						return {
							url: `/api/object/${mutationData.relatedObjectReference}/unrelate`,
							data: {
								related_object_id: mutationData.caseReference
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {

								return caseObj.merge({
									related_objects:
										caseObj.related_objects.filter(relation => relation.related_object_id !== mutationData.caseReference)
								});

							}
						);
					},
					options: {
						reloadOnComplete: true
					}

				},
				{
					type: 'case/relation/scheduled_job/add',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								case_uuid: propCheck.string,
								casetype_uuid: propCheck.string,
								job: propCheck.string,
								next_run: propCheck.string,
								interval_period: propCheck.string,
								interval_value: propCheck.number,
								runs_left: propCheck.number,
								copy_relations: propCheck.bool
							}),
							mutationData
						);

						return {
							url: '/api/v1/scheduled_job/create',
							data: assign({ type: 'scheduled_job' }, mutationData)
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {

								return caseObj.merge({
									related_objects:
										caseObj.related_objects.concat({
											related_object: {
												values: pick(mutationData, 'job', 'next_run', 'interval_value', 'interval_period', 'runs_left')
											},
											related_object_id: shortid(),
											related_object_type: 'scheduled_job',
											relationship_name_a: 'related',
											relationship_name_b: 'related',
											blocks_deletion: false,
											owner_object_id: null
										})
								});

							}
						);

					},
					options: {
						reloadOnComplete: true
					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'Geplande zaak wordt aangemaakt',
							{
								promise,
								then: ( ) => {

									let actions = [];

									if ($state.current.name.indexOf('case.relations') === -1) {
										actions = [
											{
												type: 'link',
												label: 'Relaties bekijken',
												link: $state.href('case.relations', null, { inherit: true })
											}
										];
									}

									return {
										message: 'Geplande zaak is aangemaakt',
										actions
									};
								},
								catch: ( ) => 'Geplande zaak kon niet worden aangemaakt. Neem contact op met uw beheerder voor meer informatie.'
							}
						);

					}
				},
				{
					type: 'case/relation/scheduled_job/update',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								reference: propCheck.string,
								values: propCheck.shape({
									next_run: propCheck.string,
									interval_period: propCheck.string,
									interval_value: propCheck.number,
									runs_left: propCheck.number,
									copy_relations: propCheck.bool
								})
							}),
							mutationData
						);

						return {
							url: `/api/v1/scheduled_job/${mutationData.reference}/update`,
							data: {
								instance: assign({ type: 'scheduled_job' }, mutationData.values)
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {

								return caseObj.merge({
									related_objects:
										caseObj.related_objects.map(
											relation => {

												if (relation.related_object_id === mutationData.reference) {

													return relation.merge({
														related_object: {
															values: mutationData.values
														}
													}, { deep: true });

												}

												return relation;

											}
										)
								});

							}
						);

					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'',
							{
								collapse: 0,
								promise,
								catch: ( ) => 'Geplande zaak kon niet worden bewerkt. Neem contact op met uw beheerder voor meer informatie.'
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'case/relation/scheduled_job/remove',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								reference: propCheck.string
							}),
							mutationData
						);

						return {
							url: `/api/v1/scheduled_job/${mutationData.reference}/delete`
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {

								return caseObj.merge({
									related_objects:
										caseObj.related_objects.filter(relation => relation.related_object_id !== mutationData.reference)
								});

							}
						);

					},
					options: {
						reloadOnComplete: true
					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'Geplande zaak wordt verwijderd',
							{
								promise,
								then: ( ) => 'Geplande zaak is verwijderd',
								catch: ( ) => 'Geplande zaak kon niet verwijderd worden. Neem contact op met uw beheerder voor meer informatie.'
							}
						);

					}
				},
				{
					type: 'case/relation/subject/add',
					request: ( mutationData ) => {

						if (mutationData.betrokkeneType === 'medewerker') {
							propCheck.throw(
								propCheck.shape({
									caseId: propCheck.number,
									betrokkene_identifier: propCheck.string,
									magic_string_prefix: propCheck.string,
									role: propCheck.string,
									notify_subject: propCheck.bool,
									employee_authorisation: propCheck.string,
								}),
								mutationData
							);
							return {
								url: `/api/case/${mutationData.caseId}/subjects/create`,
								data: {
									betrokkene_identifier: mutationData.betrokkene_identifier,
									magic_string_prefix: mutationData.magic_string_prefix,
									role: mutationData.role,
									notify_subject: !!mutationData.notify_subject,
									employee_authorisation: mutationData.employee_authorisation,
								}
							};
						}
						else {
							propCheck.throw(
								propCheck.shape({
									caseId: propCheck.number,
									betrokkene_identifier: propCheck.string,
									magic_string_prefix: propCheck.string,
									role: propCheck.string,
									notify_subject: propCheck.bool,
									pip_authorized: propCheck.bool,
								}),
								mutationData
							);
							return {
								url: `/api/case/${mutationData.caseId}/subjects/create`,
								data: {
									betrokkene_identifier: mutationData.betrokkene_identifier,
									magic_string_prefix: mutationData.magic_string_prefix,
									role: mutationData.role,
									notify_subject: !!mutationData.notify_subject,
									pip_authorized: !!mutationData.pip_authorized,
								}
							};
						}
					},

					reduce: ( data, mutationData ) => {

						return data.concat(
							assign(
								{
									id: shortid(),
									betrokkene_id: mutationData.betrokkene_identifier.split('-')[2],
									betrokkene_type: mutationData.betrokkene_identifier.split('-')[1]
								},
								mutationData
							)
						);

					},
					wait: ( mutationData, promise ) => {

						let actions = [];

						if ($state.current.name.indexOf('case.relations') === -1) {

							actions = [
								{
									type: 'link',
									link: $state.href('case.relations', null, { inherit: true }),
									label: 'Relaties bekijken'
								}
							];

						}

						return snackbarService.wait(
							'Betrokkene wordt toegevoegd.',
							{
								promise,
								then: ( ) => {
									return {
										message: 'Betrokkene is toegevoegd aan het zaakdossier.',
										actions
									};
								},
								catch: ( ) => 'Betrokkene kon niet worden toegevoegd aan het zaakdossier. Neem contact op met uw beheerder voor meer informatie.'
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'case/relation/subject/remove',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								subjectId: propCheck.number
							}),
							mutationData
						);

						return {
							url: `/api/case/${mutationData.caseId}/subjects/${mutationData.subjectId}/delete`
						};

					},
					reduce: ( data, mutationData ) => {

						return data.filter(subject => subject.id !== mutationData.subjectId);

					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'Betrokkene wordt verwijderd',
							{
								collapse: 0,
								promise,
								catch: ( ) => 'Betrokkene kon niet worden verwijderd. Neem contact op met uw beheerder voor meer informatie.'
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'case/relation/subject/update',
					request: ( mutationData ) => {

						if (mutationData.betrokkene_type === 'medewerker') {
							propCheck.throw(
								propCheck.shape({
									caseId: propCheck.number,
									betrokkene_identifier: propCheck.string,
									magic_string_prefix: propCheck.string,
									role: propCheck.string,
									notify_subject: propCheck.bool,
									employee_authorisation: propCheck.string,
								}),
								mutationData
							);
							return {
								url: `/api/case/${mutationData.caseId}/subjects/${mutationData.subjectId}/update`,
								data: {
									betrokkene_identifier: mutationData.betrokkene_identifier,
									magic_string_prefix: mutationData.magic_string_prefix,
									role: mutationData.role,
									notify_subject: !!mutationData.notify_subject,
									employee_authorisation: mutationData.employee_authorisation,
								}
							};
						}
						else {
							propCheck.throw(
								propCheck.shape({
									caseId: propCheck.number,
									betrokkene_identifier: propCheck.string,
									magic_string_prefix: propCheck.string,
									role: propCheck.string,
									notify_subject: propCheck.bool,
									pip_authorized: propCheck.bool,
								}),
								mutationData
							);
							return {
								url: `/api/case/${mutationData.caseId}/subjects/${mutationData.subjectId}/update`,
								data: {
									betrokkene_identifier: mutationData.betrokkene_identifier,
									magic_string_prefix: mutationData.magic_string_prefix,
									role: mutationData.role,
									notify_subject: !!mutationData.notify_subject,
									pip_authorized: !!mutationData.pip_authorized,
								}
							};
						}
					},
					reduce: ( data, mutationData ) => {

						return data.map(
							subject => {

								if (subject.id === mutationData.subjectId) {
									return subject.merge(
										omit(mutationData, 'subjectId', 'caseId')
									);
								}

								return subject;
							}
						);

					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'Betrokkene wordt bewerkt',
							{
								collapse: 0,
								promise,
								catch: ( ) => 'Betrokkene kon niet worden bewerkt. Neem contact op met uw beheerder voor meer informatie.'
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'case/relation/reschedule',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number
							}),
							mutationData
						);

						return {
							url: `/zaak/${mutationData.caseId}/reschedule/?action=update`
						};
					},
					reduce: ( data/*, mutationData*/ ) => data,
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'E-mails worden opnieuw ingepland',
							{
								promise,
								collapse: 0,
								then: ( ) => 'E-mails zijn opnieuw ingepland',
								catch: ( ) => 'E-mails konden niet opnieuw worden ingepland. Neem contact op met uw beheerder voor meer informatie'
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				}
			];

		}])
		.run([ 'mutationService', 'caseRelationActions', ( mutationService, actions ) => {

			actions.forEach(action => {
				mutationService.register(action);
			});

		}])
		.name;

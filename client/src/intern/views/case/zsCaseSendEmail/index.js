import angular from 'angular';
import vormFieldsetModule from '../../../../shared/vorm/vormFieldset';
import resourceModule from '../../../../shared/api/resource';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import vormValidatorModule from '../../../../shared/vorm/util/vormValidator';
import snackbarServiceModule from '../../../../shared/ui/zsSnackbar/snackbarService';
import emailPreviewModule from '../../../../shared/ui/zsEmailPreview';
import controller from './CaseSendEmailController';
import template from './template.html';
import './styles.scss';

export default angular
	.module('zsCaseSendEmail', [
		vormFieldsetModule,
		composedReducerModule,
		vormValidatorModule,
		snackbarServiceModule,
		resourceModule,
		emailPreviewModule
	])
	.component('zsCaseSendEmail', {
		bindings: {
			caseId: '&',
			templates: '&',
			onEmailSend: '&',
			requestor: '&'
		},
		template,
		controller
	})
	.name;

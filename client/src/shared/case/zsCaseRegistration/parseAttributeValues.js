import transformAttributeValues from './transformAttributeValues';

export default ( fields, values ) => {
	return transformAttributeValues(fields, values, 'parsers');
};

[%
rechtsvormmap = ZCONSTANTS.kvk_rechtsvormen;
rechtsvormenabled = ZCONSTANTS.kvk_rechtsvormen_enabled;
%]

<div class="form zsaction-container">

    <div class="stap-tekst">

        [% IF aanvrager_bsn %]
        <div class="system-message">
            Helaas hebben wij uw burgerservicenummer met de bijhorende persoonsgegevens niet kunnen vinden.
            [% IF !betrokkene.geslachtsnaam %]
                Wij verzoeken u vriendelijk uw gegevens in te vullen.
            [% ELSE %]
                Wij hebben uw gegevens opgehaald via een zogenaamde GBA-V
                bevraging, gelieve uw gegevens hieronder te controleren op
                correctheid.
            [% END %]
            [% IF betrokkene.messages.briefadres %]
                LET OP: U maakt gebruik van uw briefadres. Mochten deze gegevens niet correct zijn, gelieve contact op te nemen met uw gemeente.
            [% END %]
        </div>
        [% END %]

        <div class="form-required-fields-explanation"> * = Verplicht veld </div>

    </div>
    
    <div class="row">
        <input type="hidden" name="betrokkene_type" value="bedrijf">
        <input type="hidden" name="create" value="1">
        <div class="column large-3"><label class="titel">KvK-nummer *</label>
        </div>
        <div class="column large-6">
            <input
                type="hidden"
                name="dossiernummer"
                value="[% aanvrager_kvk_dossiernummer %]"
            />
            [% aanvrager_kvk_dossiernummer %]
     </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw KvK-nummer in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    
    <div class="row">
        <div class="column large-3"><label class="titel">Vestigingsnummer</label>
        </div>
        <div class="column large-6">
            <input
                type="hidden"
                name="vestigingsnummer"
                value="[% aanvrager_kvk_vestigingsnummer %]"
            />
            [% aanvrager_kvk_vestigingsnummer %]
     </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw vestigingsnummer in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    
    <div class="row">
        <div class="column large-3"><label>Handelsnaam *</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="handelsnaam"
                class="input_large"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw handelsnaam in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-3"><label>Rechtsvorm *</label></div>
        <div class="column large-6">
            [% FOREACH rechtsvormid IN rechtsvormenabled %]
                <input
                    type="radio"
                    name="rechtsvorm"
                    value="[% rechtsvormid %]"
                    'checked="checked"' : ''
                    ) %]
                    >
                    [% rechtsvormmap.$rechtsvormid %] <br>
            [% END %]
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw rechtsvorm in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="column large-3"><label>Land *</label></div>
        <div class="column large-6">
            <select
            name="vestiging_landcode"
            class="
            zsaction
            zsaction-when-6030-show-binnenland
            zsaction-when-6030-hide-buitenland
            zsaction-whennot-6030-show-buitenland
            zsaction-whennot-6030-hide-binnenland
            replace-select-start-zaak
            ">
            [% FOREACH betrokkene_option IN landcodes %]
                <option
                value="[% betrokkene_option.value %]"
                [% (betrokkene.vestiging_landcode == betrokkene_option.value || !betrokkene.landcode && betrokkene_option.value == 6030) ? 'selected="selected"' : '' %]
                >
                    [% betrokkene_option.label %]
                </option>
            [% END %]
            </select>
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw land in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Vestiging straat *</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="vestiging_straatnaam"
                class="input_large"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul de straat van uw vestiging in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Vestiging huisnummer *</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="vestiging_huisnummer"
                class="input_mini"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul het huisnummer van uw vestiging in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Vestiging huisletter</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="vestiging_huisletter"
                class="input_mini"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul de huisletter van uw vestiging in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Vestiging toevoeging</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="vestiging_huisnummertoevoeging"
                class="input_mini"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul de toevoeging van het huisnummer van uw vestiging in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Vestiging postcode *</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="vestiging_postcode"
                class="input_large"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul de postcode van uw vestiging in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-binnenland">
        <div class="column large-3"><label>Vestiging woonplaats *</label></div>
        <div class="column large-6">
            <input
                type="text"
                name="vestiging_woonplaats"
                class="input_large"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul de woonplaats van uw vestiging in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-buitenland">
        <div class="column large-3"><label>Adresregel 1 *</label></div>
        <div class="column large-6">
            <input type="text" name="vestiging_adres_buitenland1" class="input_medium" value="[% betrokkene.vestiging_adres_buitenland1 %]" />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw buitenlandse adres in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-buitenland">
        <div class="column large-3"><label>Adresregel 2</label></div>
        <div class="column large-6">
            <input type="text" name="vestiging_adres_buitenland2" class="input_medium" value="[% betrokkene.vestiging_adres_buitenland2 %]" />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw buitenlandse adres in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row zsaction-dest-buitenland">
        <div class="column large-3"><label>Adresregel 3</label></div>
        <div class="column large-6">
            <input type="text" name="vestiging_adres_buitenland3" class="input_medium" value="[% betrokkene.vestiging_adres_buitenland3 %]" />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw buitenlandse adres in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="column large-3"><label>Telefoonnummer
            [%- IF zaaktype_node.contact_info_phone_required %]*[% END -%]
        </label></div>
        <div class="column large-6">
            <input
                type="text"
                name="npc-telefoonnummer"
                value="[% aanvrager.telefoonnummer %]"
                class="input_large"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw telefoonnummer in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-3"><label>Telefoonnummer (mobiel)
            [%- IF zaaktype_node.contact_info_mobile_phone_required %]*[% END -%]
        </label></div>
        <div class="column large-6">
            <input
                type="text"
                name="npc-mobiel"
                value="[% aanvrager.mobiel %]"
                class="input_large"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw mobiele nummer in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-3"><label>E-mailadres
            [%- IF zaaktype_node.contact_info_email_required %]*[% END -%]
        </label></div>
        <div class="column large-6">
            <input
                type="text"
                name="npc-email"
                value="[% aanvrager.email %]"
                class="input_large"
            />
        </div>
        <div class="column large-3">
            [% PROCESS widgets/general/validator.tt %]
            <div class="tooltip-test-wrap">
                <div class="tooltip-test rounded">
                    Vul uw e-mailadres in.
                </div>
                <div class="tooltip-test-tip"></div>
            </div>
        </div>
    </div>
    <div class="form-required-fields-explanation"> * = Verplicht veld </div>
</div>

package Zaaksysteem::Test::AppointmentProvider::Lamson;
use Zaaksysteem::Test;

use DateTime;
use JSON::XS;
use Test::LWP::UserAgent;
use Zaaksysteem::Object::Types::Appointment;
use Zaaksysteem::Object::Types::Person;
use Zaaksysteem::Object::Types::Subject;
use Zaaksysteem::AppointmentProvider::Lamson;

sub test_book_appointment {
    my $ua = Test::LWP::UserAgent->new();

    $ua->map_response(
        qr#Appointments/CreateCustomer# => HTTP::Response->new(
            '200',
            'OK',
            [],
            '31337'
        ),
    );
    $ua->map_response(
        qr#Appointments/SaveAppointments# => HTTP::Response->new(
            '200',
            'OK',
            [],
            encode_json({
                'AppointmentGuid' => '4f38e72a-2bf7-43ef-bcf0-c56354a49772',
                'BookingNumber' => '666',
            }),
        ),
    );

    my $lamson = Zaaksysteem::AppointmentProvider::Lamson->new(
        lamsonv1_endpoint => 'https://nowhere',
        lamsonv1_ca_certificate_use_system => 1,
        lamsonv1_ca_certificate => undef,
        lamsonv1_channel => 42,
        lamsonv1_locations => [],
        ua => $ua,
    );

    my $start = DateTime->now()->set_time_zone('US/Eastern');
    my $end = $start->clone->add(hours => 1);

    my $appointment_data = {
        plugin_data => {
            product_id    => '10',
            department_id => '20',
            location_id   => '30',
        },
        start_time => $start,
        end_time   => $end,
    };
    my $subject = Zaaksysteem::Object::Types::Person->new(
        first_names   => 'first_names',
        surname       => 'surname',
        initials      => 'initials',
        prefix        => 'prefix',
        email_address => 'email_address',
    );
    my $requestor = Zaaksysteem::Object::Types::Subject->new(
        subject_type => 'person',
        subject      => $subject,
    );

    my $rv = $lamson->book_appointment($appointment_data, $requestor);

    isa_ok($rv, 'Zaaksysteem::Object::Types::Appointment');
    is(
        $rv->start_time,
        $start->set_time_zone('UTC'),
        'Start time saved to object correctly',
    );
    is(
        $rv->end_time,
        $end->set_time_zone('UTC'),
        'End time saved to object correctly',
    );
    is(
        $rv->plugin_type,
        'lamsonv1',
        'Plugin type set correctly in object',
    );

    cmp_deeply(
        $rv->plugin_data,
        {
            AppointmentGuid => '4f38e72a-2bf7-43ef-bcf0-c56354a49772',
            BookingNumber   => '666',
            product_id      => '10',
            department_id   => '20',
            location_id     => '30',
        }
    );

    my $last_request = $ua->last_http_request_sent;

    my $content = decode_json($last_request->content);

    cmp_deeply(
        $content,
        {
            'DepartmentId'           => 20,
            'CustomerId'             => 31337,
            'StartTime'              => $start->set_time_zone('UTC')->strftime('%F %T+00:00'),
            'NumberOfPersons'        => 1,
            'ProductId'              => 10,
            'SelectedMultiProdUsers' => '',
            'EndTime'                => $end->set_time_zone('UTC')->strftime('%F %T+00:00'),
            'LocationId'             => 30,
            'appointmentFields'      => {
                'controls' => [
                    {
                        'Required'     => 'true',
                        'CustomerData' => 'true',
                        'Name'         => 'gender',
                        'Label'        => 'gender',
                        'Text'         => 1
                    },
                    {
                        'Text'         => 'first_names',
                        'Label'        => 'voornaam',
                        'Name'         => 'firstname',
                        'CustomerData' => 'true'
                    },
                    {
                        'Name'         => 'lastname',
                        'CustomerData' => 'true',
                        'Label'        => 'achternaam',
                        'Text'         => 'surname'
                    },
                    {
                        'Name'         => 'prefix',
                        'CustomerData' => 'true',
                        'Text'         => 'initials',
                        'Label'        => 'voorletter'
                    },
                    {
                        'CustomerData' => 'true',
                        'Name'         => 'infix',
                        'Text'         => 'prefix',
                        'Label'        => 'tussenvoegsel'
                    },
                    {
                        'CustomerData' => 'true',
                        'Name'         => 'email',
                        'Label'        => 'E-mailadres',
                        'Text'         => 'email_address'
                    }
                ]
            }
        }
    );
}

1;

__END__

=head1 NAME

Zaaksysteem::Test::AppointmentProvider::Lamson - Test the Lamson appointment provider module

=head1 DESCRIPTION

Test Lamson appointment module

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::AppointmentProvider::Lamson

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

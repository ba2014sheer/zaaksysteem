package Zaaksysteem::Test::View::API::v1;

use Zaaksysteem::Test;

use BTTW::Tools qw[assert_profile];
use JSON::XS qw[decode_json];
use Zaaksysteem::Types qw[JSONBoolean];
use Zaaksysteem::View::API::v1;

sub _process_view;

sub test_serializer_missing_result {
    my ($status, $content_type, $result) = _process_view;

    is $status, 500, 'view processing wihout result sets status 500';

    is $result->{ type }, 'exception', 'result is an exception';
    is $result->{ instance }{ type }, 'api/v1/serializer/result_not_set', 'exception has expected type';
}

sub test_serializer_undefined_result {
    my ($status, $content_type, $result) = _process_view({ result => undef });

    is $status, 500, 'view processing wihout result sets status 500';

    is $result->{ type }, 'exception', 'result is an exception';
    is $result->{ instance }{ type }, 'api/v1/serializer/result_not_defined', 'exception has expected type';
}

sub test_serializer_preserialized_result {
    my $bare_bones_object = { type => 'abc', instance => { } };
    my $result = _process_view({ result => $bare_bones_object });

    is_deeply $bare_bones_object, $result, 'response data object same as input';
}

sub _process_view {
    my $stash = shift // {};
    my $config = shift // {};

    $stash->{ request_id } //= 'testabc-123';
    $config->{ otap } //= 'dev';

    my ($status, $content_type, $body) = (200);

    my $view = Zaaksysteem::View::API::v1->new;

    my $env = mock_one(
        stash => $stash,
        config => $config,
        res => mock_one(
            status => sub { $status = shift if @_; $status },
            content_type => sub { $content_type = shift if @_; $content_type; },
            body => sub { $body = shift if @_; $body }
        )
    );

    $view->process($env);

    my $data = eval { decode_json($body) };

    ok defined $data, 'view processing set a json body';
    ok defined $content_type, 'view processing set a content type';

    lives_ok {
        assert_profile($data, profile => {
            required => {
                request_id => 'Str', # request_id must be present, but may be unset
                development => JSONBoolean,
                api_version => 'Int',
                status_code => 'Int',
                result => 'HashRef'
            }
        });
    } 'top level json structure valid';

    return $data->{ result } unless wantarray;
    return $status, $content_type, $data->{ result };
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

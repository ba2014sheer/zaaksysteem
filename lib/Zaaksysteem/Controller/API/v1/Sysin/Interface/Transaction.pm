package Zaaksysteem::Controller::API::v1::Sysin::Interface::Transaction;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Sysin::Interface::Transaction

=head1 DESCRIPTION

This controller provides API endpoints for transaction CRUD operations.

The base path for this API is C</api/v1/sysin/interface/[UUID]/transaction>.

=cut

use BTTW::Tools;
use Zaaksysteem::Constants::Users qw[ADMIN];
use Zaaksysteem::Types qw[
    TransactionDirection
    UUID
];

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('extern');

    return;
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/sysin/interface/[UUID]/transaction> routing namespace.

=cut

sub base : Chained('/api/v1/sysin/interface/instance_base') : PathPart('transaction') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_user(ADMIN);

    assert_profile($c->stash, profile => {
        required => { interface => 'Zaaksysteem::Backend::Sysin::Interface::Component' }
    });

    $c->stash->{ transaction_rs } = $c->stash->{ interface }->transactions;

    return;
}

=head2 instance_base

Reserves the C</api/v1/sysin/interface/[UUID]/transaction/[UUID]> routing
namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $transaction_id) = @_;

    unless (UUID->check($transaction_id)) {
        throw('api/v1/sysin/interface/transaction/invalid_id', sprintf(
            'Provided transaction id "%s" is not a valid UUID',
            $transaction_id
        ));
    }

    $c->stash->{ transaction_id } = $transaction_id;

    my $transaction = $c->stash->{ transaction_rs }->search({
        uuid => $transaction_id
    })->first;

    unless (defined $transaction) {
        throw('api/v1/sysin/interface/transaction/not_found', sprintf(
            'Could not find transaction by id "%s"',
            $transaction_id
        ));
    }

    $c->stash->{ transaction } = $transaction;

    return;
}

=head2 record_base

Reserves the C</api/v1/sysin/interface/[UUID]/transaction/[UUID]/record>
routing namespace.

=cut

sub record_base : Chained('instance_base') : PathPart('record') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ transaction_record_rs } = $c->stash->{ transaction }->transaction_records;

    return;
}

=head2 record_instance_base

=cut

sub record_instance_base : Chained('record_base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $transaction_record_id) = @_;

    unless (UUID->check($transaction_record_id)) {
        throw('api/v1/sysin/interface/transaction/record/invalid_id', sprintf(
            'Provided transaction record id "%s" is not a valid UUID',
            $transaction_record_id
        ));
    }

    $c->stash->{ transaction_record_id } = $transaction_record_id;

    my $transaction_record = $c->stash->{ transaction_record_rs }->search({
        uuid => $transaction_record_id
    })->first;

    unless (defined $transaction_record) {
        throw('api/v1/sysin/interface/transaction/record/not_found', sprintf(
            'Could not find transaction record by id "%s"',
            $transaction_record_id
        ));
    }

    $c->stash->{ transaction_record } = $transaction_record;

    return;
}

=head2 list

=head3 URL Path

C</api/v1/sysin/interface/[UUID]/transaction>

=cut

sub list : Chained('base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    my $rs = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->stash->{ transaction_rs }
    );

    $rs->init_paging($c->request);

    $c->stash->{ result } = $rs;

    return;
}

=head2 create

=head3 URL Path

C</api/v1/sysin/interface/[UUID]/transaction/create>

=cut

define_profile create => (
    required => {
        external_transaction_id => 'Str',
        input_data => 'Str',
    },
    optional => {
        direction => TransactionDirection,
        processed => 'Bool',
    },
    defaults => {
        direction => 'incoming',
        processed => 0,
    }
);

sub create : Chained('base') : PathPart('create') : Args(0) {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;

    my $txn = $c->stash->{ transaction_rs }->create($args);

    # Reload from DB so we get a valid UUID
    $c->stash->{ result } = $txn->discard_changes;

    return;
}

=head2 instance_get

=head3 URL Path

C</api/v1/sysin/interface/[UUID]/transaction/[UUID]>

=cut

sub instance_get : Chained('instance_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ transaction };

    return;
}

define_profile instance_update => (
    optional => {
        external_transaction_id => 'Str',
        input_data => 'Str',
        preview_string => 'Str',
        direction => TransactionDirection,
        processed => 'Bool',
    },
);

sub instance_update : Chained('instance_base') : PathPart('update') : Args(0) {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;

    $c->stash->{ result } = $c->stash->{ transaction }->update($args);

    return;
}

=head2 record_list

=head3 URL Path

C</api/v1/sysin/interface/[UUID]/transaction/[UUID]/record>

=cut

sub record_list : Chained('record_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    my $rs = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->stash->{ transaction_record_rs }
    );

    $rs->init_paging($c->request);

    $c->stash->{ result } = $rs;

    return;
}

=head2 record_create

=head3 URL Path

C</api/v1/sysin/interface/[UUID]/transaction/[UUID]/record/create>

=cut

define_profile record_create => (
    required => {
        input => 'Str',
    },
    optional => {
        output => 'Str',
        preview_string => 'Str',
        is_error => 'Bool'
    },
    defaults => {
        output => '<not available>',
        is_error => 0
    }
);

sub record_create : Chained('record_base') : PathPart('create') : Args(0) {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;

    my $txn_record = $c->stash->{ transaction_record_rs }->create($args);

    # Fetch from database to update UUID field.
    $c->stash->{ result } = $txn_record->discard_changes;

    $c->stash->{ transaction }->denormalize_record_state;

    return;
}

=head2 record_instance_get

=head3 URL Path

C</api/v1/sysin/interface/[UUID]/transaction/[UUID]/record/[UUID]>

=cut

sub record_instance_get : Chained('record_instance_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ transaction_record };

    return;
}

=head2 record_instance_update

=head3 URL Path

C</api/v1/sysin/interface/[UUID]/transaction/[UUID]/record/[UUID]/update>

=cut

define_profile record_instance_update => (
    optional => {
        input => 'Str',
        output => 'Str',
        preview_string => 'Str',
        is_error => 'Bool'
    },
);

sub record_instance_update : Chained('record_instance_base') : PathPart('update') : Args(0) {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;

    $c->stash->{ result } = $c->stash->{ transaction_record }->update($args);

    $c->stash->{ transaction }->denormalize_record_state;

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

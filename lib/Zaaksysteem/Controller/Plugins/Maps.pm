package Zaaksysteem::Controller::Plugins::Maps;
use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

sub read : Chained('/'): PathPart('plugins/maps'): Args(0) {
    my ($self, $c) = @_;

    my $bag_model = $c->model('BAG');
    my $json_data = { success => 0, addresses => [] };

    if (my $term = $c->req->params->{term}) {
        my $addresses;
        if (my $parsed_term = $bag_model->parse_search_term($term)) {
            $addresses = [
                $bag_model->get_exact(%$parsed_term)
            ];
        } else {
            # Full text address search, get suggestions
            $addresses = $bag_model->search(
                type  => 'nummeraanduiding',
                query => $term,
            );
        }

        $json_data = {
            success => 1,
            addresses => [map { $_->to_maps_result } @$addresses],
        };
    } elsif ($c->req->params->{lat} && $c->req->params->{lon}) {
        my $lat = $c->req->params->{lat};
        my $lon = $c->req->params->{lon};

        my $nearest = $bag_model->find_nearest(
            type      => 'nummeraanduiding',
            latitude  => $lat,
            longitude => $lon,
        );

        if (!$nearest) {
            return {
                success => 0,
                message => "No objects found in range of '$lat,$lon'",
            };
        }

        $json_data = {
            success => 1,
            addresses => [$nearest->to_maps_result]
        };
    }

    $c->stash->{json} = $json_data;
    $c->detach('View::JSONlegacy');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Zaaktypen::DateTimeWrapper;
use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Zaaktypen::DateTimeWrapper - Wrapper around DateTime so changing external API doesn't impact ZTB compatibility

=head1 DESCRIPTION

In the past we've had several bugs related to L<DateTime> changing its internal format.

To prevent issues like this in the future, we now store timestamps in ZTB files
using this class, and convert back and forth on export/import.

=head1 SYNOPSIS

    my $dtw = Zaaksysteem::Zaaktypen::DateTimeWrapper->new(timestamp => $unixtime);
    my $datetime = $dtw->to_datetime;

=cut

use DateTime;
use DateTime::TimeZone::UTC;

=head1 ATTRIBUTES

=head2 utc_rd_days

The UTC RD ("Rata Die" - "fixed calendar") day this timestamp represents.

=cut

has utc_rd_days => (
    isa      => 'Int',
    is       => 'ro',
    required => 1,
);

=head2 utc_rd_secs

The number of seconds into the C<utc_rd_days> day.

=cut

has utc_rd_secs => (
    isa      => 'Int',
    is       => 'ro',
    required => 1,
);

=head1 METHODS

=head2 new_from_datetime

Calls the appropriate methods on the passed L<DateTime> object and returns a
new instance of this class.

=cut

sub new_from_datetime {
    my $class = shift;
    my $datetime = shift;

    my ($rd_days, $rd_secs) = $datetime->utc_rd_values;

    return $class->new(
        utc_rd_days => $rd_days,
        utc_rd_secs => $rd_secs,
    );
}

=head2 utc_rd_values

Returns UTC Rata Die values, that can be used to create a new L<DateTime>
object representing the same moment in time.

=cut

sub utc_rd_values {
    my $self = shift;
    return ($self->utc_rd_days, $self->utc_rd_secs);
}

=head2 time_zone

Return the time zone of this  DateTime instance. Always UTC.

=cut

sub time_zone {
    return DateTime::TimeZone::UTC->new();
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

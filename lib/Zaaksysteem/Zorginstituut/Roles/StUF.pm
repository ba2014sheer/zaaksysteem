package Zaaksysteem::Zorginstituut::Roles::StUF;
use Moose::Role;

=head1 NAME

Zaaksysteem::Zorginstituut::Roles::StUF - A role which implements StUF messages

=head1 DESCRIPTION

Creates StUF 0301 messages for the GGK domain

=head1 SYNOPSIS

    package Foo;
    with 'Zaaksysteem::Zorginstituut::Roles::StUF';

    # Must implement _build_stuf_encoder
    # then in another module
    use Foo;

    my $foo = Foo->new():
    my $xml = $foo->stuf_envelope('message_name', { data => for, the => template });
    $xml = $foo->soap_envelope($xml);

    # Send SOAP request via whatever means you have at your disposal

=cut

use BTTW::Tools qw(throw);

requires qw(
    _build_stuf_encoder
);

=head1 ATTRIBUTES

=head2 soap_envelope

The soap envelope. Implemented by the builder C<_soap_envelop>.

=cut

has soap_envelope => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    builder => '_soap_envelope',
);

=head2 stuf_encoder

The StUF encoder. Must be implemented by the consumer with the builder C<_build_stuf_encoder>.

=cut

has stuf_encoder => (
    is       => 'ro',
    builder  => '_build_stuf_encoder',
);

=head1 METHODS

=head2 add_soap_envelope

Wrap whatever you feed it into a SOAP envelope

=cut

sub add_soap_envelope {
    my ($self, $xml) = @_;
    return sprintf($self->soap_envelope, $xml);
}

=head2 stuf_envelope

Create a StUF envelope based on a writer method and data

=cut

sub stuf_envelope {
    my ($self, $method, %data) = @_;

    if (!$self->stuf_encoder->can($method)) {
        throw("stufencoder/unsupported_method", "Unsupported method $method");
    }

    return $self->stuf_encoder->$method(writer => \%data);
}

sub _soap_envelope {
    my ($self, $xml) = @_;

    return q{<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
  <SOAP-ENV:Body>%s</SOAP-ENV:Body>
</SOAP-ENV:Envelope>};

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

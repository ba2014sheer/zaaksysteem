package Zaaksysteem::Zorginstituut::Provider::Acknowledge;
use Moose;

extends 'Zaaksysteem::Zorginstituut::Provider';
with 'Zaaksysteem::Zorginstituut::Roles::Provider';

=head1 NAME

Zaaksysteem::Zorginstituut::Providers::Acknowledge - Acknowledge interface profile

=head1 DESCRIPTION

Provider module to send messages to Acknowledge
Extends L<Zaaksysteem::Zorginstituut::Provider> and consumes L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head1 SYNOPSIS

    use Zaaksysteem::Zorginstituut::Provider::Acknowledge;

    my $provider = Zaaksysteem::Zorginstituut::Provider::Acknowledge->new(
        di01_endpoint => 'http:://opperschaap.net'
    );

    # Internal calls for the provider calls:
    my $req = $provider->build_request(
        endpoint      => $provider->di01_endpoint
        'soap-action' => 'washme',
        xml           => 'foo',
    );

    my $res = $provider->send_request($req);

=head2 shortname

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head2 label

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=head2 configuration_items

Defined by the consumed role L<Zaaksysteem::Zorginstituut::Roles::Provider>

=cut

sub shortname { "acknowledge" }
sub label     { "Acknowledge" }

my @CONFIGURATION_ITEMS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_acknowledge_di01_endpoint',
        label       => 'Di01 endpoint',
        description => 'De URI voor Di01 (heen) berichttypes',
        type        => 'text',
        required    => 1,
        data        => { pattern => '^https:\/\/.+' },
    ),
);

sub configuration_items {
    return @CONFIGURATION_ITEMS;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


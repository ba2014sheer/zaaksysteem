package Zaaksysteem::CLI::Knife::Controlpanel;

use Moose::Role;
use Zaaksysteem::CLI::Knife::Action;

use constant KNIFE => 'controlpanel';

my $knife = 'controlpanel';

=head1 NAME

Zaaksysteem::CLI::Knife::Controlpanel - Controlpanel CLI actions

=head1 SYNOPSIS

    See USAGE from commandline output zsknife

=cut

register_knife      $knife => (
    description => "Controlpanel related functions"
);

register_category   'cp' => (
    knife           => $knife,
    description     => "Controlpanel actions"
);

register_category   'instance' => (
    knife           => $knife,
    description     => "Instance actions"
);

register_category   'host' => (
    knife           => $knife,
    description     => "Host actions"
);

register_action     'search' => (
    knife           => $knife,
    category        => 'cp',
    description     => 'Search controlpanels',
    run             => sub {
        my $self        = shift;
        my (@params)    = @_;

        my $search      = $self->get_knife_params;

        my @objects     = $self->objectmodel->search('controlpanel', $search);

        my $msg         = '';
        for my $object (@objects) {
            $msg .= "\n********************\n";

            my ($bid)       = $object->owner =~ /betrokkene-bedrijf-(\d+)$/;
            my $np          = $self->schema->resultset('Bedrijf')->find($bid);
            $msg .= sprintf("%-30s: %s", 'Eigenaar', $np->handelsnaam . ' [' . $np->dossiernummer . ']') . "\n" if $np;

            $msg .= sprintf("%-30s: %s", $_, (ucfirst($object->$_ || ''))) . "\n" for qw/id shortname template read_only customer_reference allowed_instances allowed_diskspace/;
            $msg .= "********************\n";
        }

        print $msg;
    }
);

1;

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

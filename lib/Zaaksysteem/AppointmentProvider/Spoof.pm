package Zaaksysteem::AppointmentProvider::Spoof;
use Moose;
use namespace::autoclean;

with qw(
    Zaaksysteem::AppointmentProvider
    MooseX::Log::Log4perl
);

=head1 NAME

Zaaksysteem::AppointmentProvider::Spoof - Spoof/test mode appointment provider plugin

=head1 DESCRIPTION

This plugin returns fake (test) data for testing out the appointment provider features
of Zaaksysteem.

=cut

use DateTime;
use Zaaksysteem::Object::Types::Appointment;
use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;

=head1 ATTRIBUTES

=head2 spoof_days

Number of days into the future to return free slots for.

=cut

has spoof_days => (
    isa => 'Int',
    is => 'ro',
    required => 1,
);

=head2 slots_per_day

Number of free appointment slots to return for a given day.

=cut

has slots_per_day => (
    isa => 'Int',
    is => 'ro',
    required => 1,
);

=head1 METHODS

=head2 shortname

Always returns the string C<spoof>.

=cut

sub shortname { "spoof" }

=head2 label

Always returns the string C<Testkalender ('spoof mode')>.

=cut

sub label { "Testkalender ('spoof mode')" }

my @CONFIGURATION_ITEMS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_spoof_days',
        type        => 'text',
        label       => 'Dagen in de toekomst',
        required    => 1,
        default     => 10,
        description => '<p>Aantal dagen in de toekomst om te tonen.</p>',
        data        => { pattern => '^\d+$' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_slots_per_day',
        type        => 'text',
        label       => 'Tijdslots per dag',
        required    => 1,
        default     => 10,
        description => '<p>Aantal vrije tijdslots om te tonen voor elke dag.</p>',
        data        => { pattern => '^\d+$' },
    ),
);

=head2 configuration_items

Returns a list of configuration items (L<Zaaksysteem::ZAPI::Form::Field>
instances) to influence test mode parameters.

=cut

sub configuration_items {
    return @CONFIGURATION_ITEMS;
}

=head2 get_product_list

Retrieve a (spoof) product list, used to configure appointment plugin library
attributes.

=cut

sub get_product_list {
    my ($self, $location_id) = @_;

    $location_id =~ s/[^0-9]//g;

    return [
        { id => 1, label => "Fake product 1 (locatie $location_id)" },
        { id => 2, label => "Fake product 2 (locatie $location_id)" },
    ];
}

=head2 get_location_list

Retrieve a (spoof) location list, used to configure appointment plugin library
attributes.

=head3 Parameters

The id of the product to get a location list for. Should be an "id" as returned
by C<get_product_list>.

=cut

sub get_location_list {
    my ($self) = @_;

    return [
        { id => 1, label => "Hier" },
        { id => 2, label => "Daar" },
    ];
}

=head2 get_dates

Retrieve a list of dates on which appointments can be planned. Uses the "number
of days in the future" configuration item.

=cut

sub get_dates {
    my ($self) = @_;

    my $today = DateTime->now();

    my @rv;
    for (1 .. $self->spoof_days) {
        my $date = $today->clone->add(days => $_);

        push @rv, $date->ymd;
    }

    return \@rv;
}

=head2 get_timeslots

Retrieve a list of time slots that can be used to plan appointments. Uses the
"number of slots per day" configuration item to determine the number of slots.

=head3 Parameters

This method requires one argument: a L<DateTime> instance indicating the date
for which to get available slots.

=cut

sub get_timeslots {
    my ($self, $date) = @_;

    my $slot_length = int(86400 / $self->slots_per_day);

    my @rv;
    for (1 .. $self->slots_per_day) {
        my $end = $date->clone->add(seconds => $slot_length);

        push @rv, {
            start_time  => $date->clone->set_time_zone('UTC')->iso8601 . 'Z',
            end_time    => $end->clone->set_time_zone('UTC')->iso8601 . 'Z',
            plugin_data => { spoof_timeslot_id => rand(100) },
        };

        $date = $end;
    }

    return \@rv;
}

=head2 book_appointment

Create a new appointment. As this is the spoof module, creating will always succeed.

=head3 Parameters

One block of C<appointment_data>, in the same format as returned by
C<get_timeslots>, and one L<Zaaksysteem::Object::Types::Subject> instance (for the person
for whom he appointment is being made).

=head3 Returns

A new L<Zaaksysteem::Object::Types::Appointment> object (unsaved).

=cut

sub book_appointment {
    my ($self, $appointment_data, $requestor) = @_;

    throw(
        "appointment_provider/spoof/invalid_data",
        "Invalid spoof appointment: missing field 'spoof_timeslot_id'"
    ) unless exists $appointment_data->{plugin_data}{spoof_timeslot_id};

    $self->log->info(sprintf(
        "Spoof mode appointment created for subject '%s', start: '%s', end: '%s', plugin data: '%s'",
        $requestor->id,
        $appointment_data->{start_time}->strftime("%a %d %b %Y, %H:%M"),
        $appointment_data->{end_time}->strftime("%a %d %b %Y, %H:%M"),
        dump_terse($appointment_data->{plugin_data}),
    ));

    $appointment_data->{plugin_type} = $self->shortname;
    $appointment_data->{plugin_data}{spoof_appointment_id} = rand(100);

    my $appointment = Zaaksysteem::Object::Types::Appointment->new(%$appointment_data);

    return $appointment;
}

=head2 delete_appointment

Remove an appointment. As this is the spoof module, this will always succeed,
as long as the right format of appointment data is supplied.

=head3 Arguments

One L<Zaaksysteem::Object::Types::Appointment> instance, containing the appointment to remove.

=head3 Returns

A hash reference, containing the success state (always true in the Spoof case).

=cut

sub delete_appointment {
    my ($self, $appointment) = @_;

    $self->log->info(sprintf(
        "Spoof mode appointment deleted, start: '%s', end: '%s', plugin data: '%s'",
        $appointment->start_time->strftime("%a %d %b %Y, %H:%M"),
        $appointment->end_time->strftime("%a %d %b %Y, %H:%M"),
        dump_terse($appointment->plugin_data),
    ));

    return {
        success => \1,
    };
}

=head2 test_connection

Perform a "connection test". As this is the spoof mode implementation, made for
easy testing, this test fails 50% of the time (randomly)

=cut

sub test_connection {
    if (rand >= 0.5) {
        throw(
            'appointmentprovider/spoof/test_failed',
            'Deze test mislukt 50% van deze tijd'
        );
    }
    return;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

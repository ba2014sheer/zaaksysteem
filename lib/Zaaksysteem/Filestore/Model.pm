package Zaaksysteem::Filestore::Model;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

use List::Util qw(first none);
use Module::Pluggable::Object;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Filestore::Model - Model to handle configured file storage engines

=head1 DESCRIPTION

A model to create file storage engine instances, as specified in the global configuration.

=head1 ATTRIBUTES

=head2 storage_bucket

The name of the storage bucket ("container" in Swift, "bucket" in S3) to use.

=cut

has storage_bucket => (
    is => 'ro',
    isa => 'Str',
);

=head2 local_base_path

Base path for local file storage. Used by the "UStore" engine.

=cut

has local_base_path => (
    is  => 'ro',
    isa => 'Str',
);

=head2 engine_configuration

Array containing storage engine configurations.

The first one will be used as the "default" storage.

=cut

has engine_configuration => (
    is       => 'ro',
    isa      => 'ArrayRef',
    required => 1,
);

=head2 engines

Instances of all storage engines that have been used.

=cut

has engines => (
    is => 'ro',
    isa => 'HashRef',
    init_arg => undef,
    default => sub { {} },
);

=head1 METHODS

=head2 get_default_engine

Returns the default storage engine.

=cut

sub get_default_engine {
    my $self = shift;

    my $default = $self->engine_configuration->[0]{name};
    $self->log->trace("Request for default storage engine: '$default'");

    return $self->get_engine_by_name($default);
}

=head2 get_engine_by_name

Retrieve a storage engine by its configured name.

=cut

sig get_engine_by_name => 'Str';

sub get_engine_by_name {
    my $self = shift;
    my $name = shift;

    if (exists $self->engines->{ $name }) {
        $self->log->trace("Engine '$name' already created. Returning existing copy.");
        return $self->engines->{ $name }
    }

    my $config = first { $_->{name} eq $name } @{ $self->engine_configuration };
    throw(
        'filestore/config_not_found',
        "File stored using plugin '$name', which was not found in configuration.",
    ) unless $config;

    my @engines = $self->engine_plugins;

    if (none { $_ eq $config->{class} } @engines) {
        throw(
            'filestore/engine_not_found',
            "File storage engine '$config->{class}' configured, but not found",
        );
    }

    $self->log->trace("Creating new instance of engine '$name' of type '$config->{class}'");

    $self->engines->{ $name } = $config->{class}->new(
        %{ $config->{constructor_arguments} },

        # These values are not overrideable by "constructor arguments":
        name               => $name,
        storage_bucket     => $self->storage_bucket,
        local_storage_path => $self->local_base_path,
    );

    return $self->engines->{ $name };
}

=head2 get_configured_engine_names

Returns a list of names of the configured storage engines, for later use in "get_engine_by_name".

=cut

sub get_configured_engine_names {
    my $self = shift;
    return map { $_->{name} } @{$self->engine_configuration};
}

=head2 engine_plugins

Returns the list of all available C<Zaaksysteem::Filestore::Engine>s.

=cut

my @ENGINE_PLUGINS;

sub engine_plugins {
    if (!@ENGINE_PLUGINS) {
        my $finder = Module::Pluggable::Object->new(
            search_path => 'Zaaksysteem::Filestore::Engine',
            except      => 'Zaaksysteem::Filestore::Engine',
            require     => 1,
        );

        @ENGINE_PLUGINS = $finder->plugins;
    }
    return @ENGINE_PLUGINS;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

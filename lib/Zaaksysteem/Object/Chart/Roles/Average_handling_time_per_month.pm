package Zaaksysteem::Object::Chart::Roles::Average_handling_time_per_month;

use Moose::Role;
use Data::Dumper;

=head2 search_average_handling_time_per_month

Returns a list with the average handling time per month within the
given resultset set. Run a grouping query.

=cut

sub search_average_handling_time_per_month {
    my ($self, $rs) = @_;

    my $agg_rs = $self->resultset->search(
        {},
        {
            select => [
                { max => "hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))" },
            ],
            "as" => [ 'max' ],
            order_by => undef,
        }
    );
    $agg_rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my $minmax = $agg_rs->first;
    my $latest   = $minmax->{max};
    
    my $resultset = $rs->search({
        -and => [
            \["defined(index_hstore, 'case.date_of_completion')"],
            {
                "hstore_to_timestamp((index_hstore->'case.date_of_registration')) + INTERVAL '1 year'" => { '>=' => $latest }
            }
        ]
    }, {
        select => [
            {
                date_part => "'month', hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))",
                -as => 'month'
            },
            {
                date_part => "'year', hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))",
                -as => 'year',
            },
            {
                avg => {
                    date_part => "'days', hstore_to_timestamp((me.index_hstore->'case.date_of_completion')) - hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))"
                },
                -as => 'average_handling_time'
            }
        ],
        group_by => [ 'year', 'month'],
        order_by => [ 'year', 'month']
    });

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my @results = $resultset->all();
    return @results;
}


=head2 average_handling_time_per_month

Returns a chart profile

=cut

sub average_handling_time_per_month {
    my ($self) = @_;

    my @results = $self->search_average_handling_time_per_month($self->resultset);

    my @month_names = qw/januari februari maart april mei juni juli augustus september oktober november december/;

    my @months = map {
        $month_names[$_->{month}-1] . ' '. $_->{year}
    } @results;

    my @average_handling_times = map { int $_->{average_handling_time} } @results;

    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => 'Gemiddelde behandeltijd per maand (alleen afgehandelde zaken)',
        },
        xAxis => {
            categories => \@months
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Aantal dagen'
            }
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table border="1">',
            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
                '<td style="padding:0"><b>{point.y:.f} dagen</b></td></tr>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                pointPadding => 0.2,
                borderWidth => '0'
            }
        },
        series => [{
            name => 'Gem. behandeltijd (dagen)',
            data => \@average_handling_times
        }]
    };

    return $profile;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


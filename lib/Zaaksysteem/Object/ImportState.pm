package Zaaksysteem::Object::ImportState;

use Moose;

use Zaaksysteem::Object::ImportStateItem;

use Data::Dumper;

has 'object_type' => ( is => 'rw' );
has 'import_fileref' => ( is => 'rw' );
has 'format' => ( is => 'rw' );
has 'library_id' => ( is => 'rw' );

has 'items' => (
    is => 'rw',
    isa => 'ArrayRef[Zaaksysteem::Object::ImportStateItem]',
    default => sub { [] }
);

=head2 items_to_delete

List of FAQs/products that aren't in the file to import, and will be deleted.

=cut

has 'items_to_delete' => (
    is => 'rw',
    isa => 'ArrayRef',
    default => sub { [] },
);

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    my %args = @_;

    if(exists $args{ items } && ref $args{ items } eq 'ARRAY') {
        $args{ items } = [
            map { Zaaksysteem::Object::ImportStateItem->new($_) } @{ $args{ items } }
        ];
    }

    return $class->$orig(\%args);
};

sub new_item {
    my $self = shift;

    my $item = Zaaksysteem::Object::ImportStateItem->new(shift);

    push(@{ $self->items }, $item);

    return $item;
}

sub get_state {
    my $self = shift;

    return {
        object_type => $self->object_type,
        import_fileref => $self->import_fileref,
        format => $self->format,
        library_id => $self->library_id,
        items => [ map { $_->get_state } @{ $self->items } ],
        items_to_delete => $self->items_to_delete,
    };
}

sub get_by {
    my $self = shift;

    my ($item) = $self->find_by(@_);

    return $item;
}

sub find_by {
    my $self = shift;
    my $attribute = shift;
    my $value = shift;

    return grep { ($_->$attribute || '') eq $value } @{ $self->items };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 find_by

TODO: Fix the POD

=cut

=head2 get_by

TODO: Fix the POD

=cut

=head2 get_state

TODO: Fix the POD

=cut

=head2 new_item

TODO: Fix the POD

=cut


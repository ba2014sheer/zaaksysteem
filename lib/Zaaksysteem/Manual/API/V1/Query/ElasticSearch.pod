=head1 NAME

Zaaksysteem::Manual::API::V1::Query::Elasticsearch - Elasticsearch Query DSL
for Zaaksysteem documentation

=head1 DESCRIPTION

This page documents the usage of Zaaksysteem's Elasticsearch Query API. Using
this API, the connecting client can search for and retrieve object instances.

Zaaksysteem implements a subset of the complete ES Query DSL, since not all
features of Elasticsearch can be found in Zaaksysteem. This mostly affects
the full-text matching and result ranking use cases, which are not supported
with the exception of the L</match> query on the C<_all> keyword.

=head1 QUICK START

=head2 Elasticsearch Queries

    {
        "query": {
            "match": {
                "_all": "keywords"
            }
        }
    }

Above you can see a very simple ESQ. This query (which B<must> start with the
C<query> at the top level), simply returns all objects matching the full-text
keyword C<keyword>. Elasticsearch would expect to receive such a query on
it's REST API using a C<HTTP POST> call.

Zaaksysteem, however, already has a namespacing API with the expectation that
C<POST> requests will mutate the state of any number of objects. Due to this
expectation, Zaaksysteem accepts ESQs using URL encoding of the above query
in a C<GET> request. The above query would look like this:

    /api/endpoint&es_query=1&query:match:_all=keywords

The C<es_query> parameter, when set, indicates that Zaaksysteem should switch
to ESQ parsing for the rest of the query string.

The C<query:match:_all> parameter works as a path notation for the
datastructure above. It will be automatically inflated to the above notation,
and Zaaksysteem then uses the inflated form internally for parsing and
generating the query to storage.

A more complete example:

    {
        "query": {
            "bool": {
                "must": {
                    "match": {
                        "_all": "keyword"
                    }
                },
                "must_not": {
                    "term": {
                        "field": 13
                    }
                }
            }
        }
    }

Would look like this:

    /api/endpoint&es_query=1&query:bool:must:match:_all=keyword&query:bool:must_not:term:field=13

=head2 Supported querys

=head3 match_all

    {
        "query": {
            "match_all": {}
        }
    }

The C<match_all> query returns all objects within the object type scope of the
API. It is equivalent to an empty query.

=head3 match

=head4 ESQ

    {
        "query": {
            "match": {
                "_all": "keyword"
            }
        }
    }

=head4 Zaaksysteem URL

    /api/endpoint&es_query=1&query:match:_all=keyword

The C<match> query is partially supported. Zaaksysteem does not index every
field within objects. Instead, a global inverted keyword index is built from
predefined fields, on a per-object or per-objecttype basis. Cases for example
would contain the C<number>, C<subject>, C<requestor.full_name>, and other
fields.

Matching on this object-scope inverted index can be done via the C<_all> key
in the match query. Attempting to use a specific field to match on will result
in an exception being thrown.

=head3 term

=head4 ESQ

    {
        "query": {
            "term": {
                "field": "value"
            }
        }
    }

=head4 Zaaksysteem URL

    /api/endpoint&es_query=1&query:term:field=value

The C<term> field query supports exact value matching on fields of objects.
The value types are heuristically determined by Zaaksysteem, in most queries
it should "just work".

C<term> queries with multiple key-value pairs are implicitly AND-ed together.

=head3 range

=head4 ESQ

    {
        "query": {
            "range": {
                "field": {
                    "gte": 20,
                    "lte": 10
                }
            }
        }
    }

=head4 Zaaksysteem URL

    /api/endpoint&es_query=1&query:range:field:gte=20&query:range:field:lte=10

The C<range> query works like an implied 'must' L</bool> of two L</term>
queries.

=head3 bool

=head4 ESQ

    {
        "query": {
            "bool": {
                "must": {
                    "term": {
                        "field": "value"
                    }
                },
                "must_not": {
                    "match": {
                        "_all": "keyword"
                    }
                }
            }
        }
    }

=head4 Zaaksysteem URL

    /api/endpoint&es_query=1&query:bool:must:term:field=value&query:bool:must_not:match:_all=keyword

C<bool> queries allow the construction of more complex queries, bringing more
conditional logic to the table. In the current implementation, not all C<bool>
queries are fully supported.

Currently, C<bool:must> and C<bool:filter> perform the exact same query, since
Zaaksysteem has no notion of document ranking in search results.

The C<bool:should> query is not implemented, and an exception will be thrown
should it be used.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

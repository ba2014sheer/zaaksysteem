package Zaaksysteem::API::v1::Message::Ack;

use Moose;

extends 'Zaaksysteem::API::v1::Message';

=head1 NAME

Zaaksysteem::API::v1::Message::Ack - Simple acknowledge message

=head1 DESCRIPTION

This message is used by api/v1 to confirm a request was received, and no
exceptions occurred while processing.

Some API requests will not (immediately) produce a result, asynchronous
processes via L<Zaaksysteem::Object::Queue> for example. Instances of this
message will simply inform the client everything is OK, and they can continue.

=head1 ATTRIBUTES

=head2 message

Provides a textual explanation for the acknowledgement, if one is set.

=cut

has message => (
    is => 'rw',
    isa => 'Str',
    default => 'Request sucessfully processed.'
);

=head1 METHODS

=head2 field_hash

Override for L<Zaaksysteem::API::v1::Message/field_hash> that provides
message type and payload information for the acknowledge message.

    {
        type => 'ack',
        message => 'Some message'
    }

=cut

override field_hash => sub {
    my $self = shift;

    return {
        type => 'ack',
        message => $self->message
    }
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

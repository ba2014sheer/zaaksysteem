import {
    openPageAs
} from './../../../../functions/common/navigate';
import waitForElement from './../../../../functions/common/waitForElement';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    openRegisteredCase,
    assign
} from './../../../../functions/common/form';
import {
    getSummaryValue,
    getAboutValue
} from './../../../../functions/intern/caseView/caseMenu';
import {
    openTab
} from './../../../../functions/intern/caseView/caseNav';

const assignmentTypesInfo = [
    {
        type: 'without assignment options',
        caseType: 'Toewijzing bij registratie geen opties',
        performAssignment: () => {
            goNext();
            openRegisteredCase();
        },
        results: {
            behandelaar: 'In behandeling nemen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Backoffice'
        }
    },
    {
        type: 'to myself',
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: () => {
            assign('me', { changeDepartment: false });
            goNext();
        },
        results: {
            behandelaar: 'F. assign',
            coordinator: 'formassign',
            status: 'In behandeling',
            afdeling: 'Backoffice'
        }
    },
    {
        type: 'to myself and changing department',
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: () => {
            assign('me', { changeDepartment: true });
            goNext();
        },
        results: {
            behandelaar: 'F. assign',
            coordinator: 'formassign',
            status: 'In behandeling',
            afdeling: 'Form'
        }
    },
    {
        type: 'to a coworker',
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: () => {
            assign('coworker', { assignee: 'Toewijzing wijzigen' });
            goNext();
            openRegisteredCase();
        },
        results: {
            behandelaar: 'T. Wijzigen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Backoffice'
        }
    },
    {
        type: 'to a coworker and changing department',
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: () => {
            assign('coworker', { assignee: 'Toewijzing wijzigen', sendEmail: false, changeDepartment: true });
            goNext();
            openRegisteredCase();
        },
        results: {
            behandelaar: 'T. Wijzigen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Zaakacties'
        }
    },
    {
        type: 'to a coworker and sending an email',
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: () => {
            assign('coworker', { assignee: 'Toewijzing wijzigen', sendEmail: true, changeDepartment: false });
            goNext();
            openRegisteredCase();
        },
        results: {
            behandelaar: 'T. Wijzigen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Backoffice',
            emailed: true
        }
    },
    {
        type: 'to a department',
        caseType: 'Toewijzing bij registratie alle opties',
        performAssignment: () => {
            assign('org-unit', { department: '-Form', role: 'Contactbeheerder' });
            goNext();
            openRegisteredCase();
        },
        results: {
            behandelaar: 'In behandeling nemen',
            coordinator: '-',
            status: 'Nieuw',
            afdeling: 'Form',
            rol: 'Contactbeheerder' //this value is not available in the case view yet, so no tests have been included
        }
    }
];

assignmentTypesInfo.forEach(assignmentTypeInfo => {
    const { type, caseType, performAssignment, results } = assignmentTypeInfo;
    const { behandelaar, coordinator, status, afdeling, rol, emailed } = results;

    describe(`when starting a registration form and assigning ${type}`, () => {
        beforeAll(() => {
            const data = {
                casetype: caseType,
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            openPageAs('formassign', '/intern/');
            startForm(data);
            goNext();
            performAssignment();
        });

        it(`the case should have the assignee "${behandelaar}"`, () => {
            expect(getSummaryValue('Behandelaar')).toEqual(behandelaar);
        });

        it(`the case should have the coordinator "${coordinator}"`, () => {
            expect(getAboutValue('Coordinator')).toEqual(coordinator);
        });

        it(`the case should have the status "${status}"`, () => {
            expect(getSummaryValue('Status')).toEqual(status);
        });

        it(`the case should have the department "${afdeling}"`, () => {
            expect(getSummaryValue('Afdeling')).toEqual(afdeling);
        });

        if ( emailed ) {
            it('the assignee should have been emailed', () => {
                browser.ignoreSynchronization = true;
                openTab('timeline');
                browser.sleep(5000);
                expect($('[data-event-type="email/send"]').isPresent()).toBe(emailed);
                browser.get('/intern/');
                browser.ignoreSynchronization = false;
            });
        }
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/

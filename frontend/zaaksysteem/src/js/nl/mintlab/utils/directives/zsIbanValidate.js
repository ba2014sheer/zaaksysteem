/*global IBAN,angular,_*/
(function () {
    "use strict";
    angular.module('Zaaksysteem').directive('zsIbanValidate', [ '$parse', function ( $parse ) {

        function checkBanknumber(value) {
            return IBAN.isValid(value);
        }

        return {
            restrict: 'A',
            require: [ 'ngModel', '?^zsCaseWebformIbanField' ],
            scope: false,
            link: function (scope, element, attrs, controllers ) {
                
                var ngModel = controllers[0],
                    zsCaseWebformIbanField = controllers[1];
                
                if(zsCaseWebformIbanField) {
                    zsCaseWebformIbanField.setIbanValidate({
                        setValue: function ( value ) {
                            $parse(attrs.ngModel).assign(scope, value);
                        }
                    });
                }

                var validator = function (value) {
                    // For now required fields checking has to be done by the backend
                    // therefore empty is considered valid by this validator.
                    // $isEmpty considers NaN, undefined and null empty as well,
                    // though only empty string should count. Chose $isEmpty to
                    // follow common angularjs pattern.
                    var valid = ngModel.$isEmpty(value) || checkBanknumber(value);
                    ngModel.$setValidity('zsIbanValidate', valid);
                    return valid ? value : undefined;
                };

                ngModel.$parsers.unshift(validator);
                ngModel.$formatters.unshift(validator);
                
                if(zsCaseWebformIbanField) {
                    scope.$watch('$destroy', function ( ) {
                        zsCaseWebformIbanField.unsetIbanValidate();
                    });
                }
            }
        };
    }]);
}());
